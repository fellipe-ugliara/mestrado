## Projeto

Essa pasta contém; os programas Cyan ( [app](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/app/) ), que foram desenvolvidos; os arquivos JARs ( [jar](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/jar/) ), que são as bibliotecas Java utilizadas; as bibliotecas Cyan ( [lib](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/lib/) ), que foram utilizadas e desenvolvidas; e código-fonte do compilador de Cyan ( [proj](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/proj/) ), que foi usado durante essa pesquisa.

Na pasta [app](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/app/) estão disponíveis as informações de como compilar e executar os programas Cyan desenvolvidos. E na pasta [proj](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/proj/) é explicado como gerar um novo JAR do compilador de Cyan.

No site de [Cyan](http://cyan-lang.org/) outras versões do compilador de Cyan estão disponíveis. Caso elas sejam usadas, os programas e as bibliotecas provavelmente precisaram ser adaptados.