## JARs (Java ARchive)

Essa pasta contém os arquivos JARs externos adicionados ao projeto. Eles são bibliotecas Java usadas na implementação do compilador de Cyan. Os arquivos JARs adicionados são:

- commons-io: o Commons IO é uma biblioteca de utilitários para auxiliar no desenvolvimento da funcionalidade de IO [(Link)](https://commons.apache.org/proper/commons-io/);
- commons-lang: utilitários auxiliares para a API java.lang, fornece manipulação de strings, métodos numéricos básicos, reflexão de objeto, simultaneidade, serialização e propriedades do sistema [(Link)](https://commons.apache.org/proper/commons-lang/);
- gson: é uma biblioteca Java que pode ser usada para converter objetos Java em sua representação JSON [(Link)](https://github.com/google/gson);
- java-json: fornece APIs portáteis que permitem analisar, gerar, transformar e consultar JSON [(Link)](http://www.oracle.com/technetwork/pt/articles/java/api-java-para-json-2251326-ptb.html);
- javassist: fornecendo meios para manipular o bytecode Java de um programa Java. O javassist fornece o suporte para a reflexão estrutural, ou seja, a capacidade de alterar a implementação de uma classe em tempo de execução [(Link)](http://jboss-javassist.github.io/javassist/);
- jgraphx: fornece funcionalidade para visualizar e interagir com gráficos [(Link)](https://github.com/jgraph/jgraphx);
- jtidy: é um porte para Java do HTML Tidy, um verificador de sintaxe para HTML [(Link)](http://jtidy.sourceforge.net/);
- reflections: determinar métodos e atributos que serão utilizados de determinada classe em tempo de execução [(Link)](http://www.oracle.com/technetwork/articles/java/javareflection-1536171.html);
- saci: compilador de Cyan gerado por esse projeto [(Link)](http://cyan-lang.org/);
- slf4j: facilita a criação de logs em Java [(Link)](https://www.slf4j.org/);
- treplica: framework utilizado para desenvolver aplicação replicadas [(Link)](https://bitbucket.org/gdvieira/treplica/src/master/).

