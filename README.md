## Mestrado

Este repositório contém a dissertação de mestrado **Replicação Orientada a Metaprogramação**, ele está dividido em duas pastas.  Na pasta [dissertacao](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/dissertacao/), está o texto da dissertação, que foi escrita usando a linguagem LaTeX. E na pasta [projeto](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/), estão os arquivos que foram usados e criados durante essa pesquisa.

Na pasta [projeto/app](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/app/) é descrito como compilar e executar os programas presentes na dissertação. O código-fonte do compilador Cyan usado nessa dissertação está na pasta [projeto/proj](https://bitbucket.org/fellipe-ugliara/mestrado/src/master/projeto/app/), nessa pasta existe um tutorial de como compilar o compilador de Cyan caso você julgue necessário.

Essa pesquisa está associada fortemente aos projetos [Treplica](https://bitbucket.org/gdvieira/treplica/src/master/) e [Cyan](http://cyan-lang.org/)

Qualquer dúvida entre em contato pelo e-mail [ugliara.fellipe@gmail.com](ugliara.fellipe@gmail.com). Ficarei feliz em ajudar!
